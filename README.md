# WIP - NixOS Bigdata Environment

Project based on https://github.com/nix-community/templates/tree/main/python, including some python packages to work with Big Data.

Also, I was having issues running `nix-shell` command, or even using direnv as I use zsh, but thanks to this plugin: https://github.com/chisui/zsh-nix-shell, finally it's working.


## Usage:

Clone and then rename it to your preject name.

TODO - Trying to either create a installer or make it work with `nix flake init -t <git link>`


As I use direnv, (https://github.com/nix-community/nix-direnv), I just need to run `direnv allow` to use the environment.

Then open it in your editor and choose the ".venv" environment created.

## Packages:
Edit file `flake.nix` to add or remove packages
- python 3.11.7

- Basic / build
    - pip
    - setuptools
    - wheel
    - requests

- Dev
    - pre-commit
    - black
    - flake8
    - isort
    - mypy
    - pylint
    - autopep8

- Test
    - pytest
    - pytest-cov

- Doc:
    - sphinx

- Packaging
    - twine

- Big Data and other modules:
    - beautifulsoup4
    - pyspark
    - sqlalchemy
    - tensorflow
    - numpy
    - scipy
    - pandas
    - matplotlib
    - scikit-learn
    - pytorch
    - scrapy
    - tox
    - lightgbm

## License
- MIT
