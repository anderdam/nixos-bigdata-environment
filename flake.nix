{
  description = "Python Big Data Environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};

        python = pkgs.python311;

        nativeBuildInputs = with pkgs; [
          python
          pre-commit
        ];

        buildInputs = with python.pkgs; [
          #Basic / build
          pip
          setuptools
          wheel
          requests
          beautifulsoup4

          #Dev
          flake8
          black
          isort
          mypy
          pylint
          autopep8

          #Test
          pytest
          pytest-cov

          #Doc
          sphinx

          #CI
          tox

          #Packaging
          twine

          #Big Data
          pyspark
          sqlalchemy
          tensorflow
          numpy
          scipy
          pandas
          matplotlib
          scikit-learn
          pytorch
          scrapy
          lightgbm
        ];

      in {
        devShells.default = pkgs.mkShell rec { inherit nixpkgs nativeBuildInputs buildInputs; };

        packages.default = python.pkgs.buildPythonApplication {
          pname = "template";
          version = "0.0.0";
          format = "setuptools";

          src = ./.;

          # True if tests
          doCheck = false;

          shellHooks = ''
            echo "Welcome to Big Data Environment!"
            pre-commit install
          '';

          inherit nativeBuildInputs buildInputs;
        };
      }
    );
}
